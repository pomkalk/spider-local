import Phaser from 'phaser'

export default {
  type: Phaser.AUTO,
  parent: 'game-view',
  width: 1024,
  height: 768
}
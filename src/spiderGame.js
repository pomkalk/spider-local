import Phaser from 'phaser'
import config from './config/config'
import BootScene from './scenes/boot';
import PreloaderScene from './scenes/preloader';
import MenuScene from './scenes/menu';
import GameScene from './scenes/game';
import ScoresScene from './scenes/scores';

export default class SpiderGame extends Phaser.Game {
  constructor() {
    super(config)
    this.scene.add('Boot', BootScene)
    this.scene.add('Preloader', PreloaderScene)
    this.scene.add('Menu', MenuScene)
    this.scene.add('Game', GameScene)
    this.scene.add('Scores', ScoresScene)

    this.scene.start('Boot')
  }
}

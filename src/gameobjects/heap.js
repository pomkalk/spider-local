import Phaser from 'phaser'
import CardEmptySprite from '../sprites/CardEmptySprite'
import CardBackSprite from '../sprites/CardBackSprite'
import CardSprite from '../sprites/Card'

export default class Heap extends Phaser.GameObjects.Container {
  constructor(scene, x, y, cb = function(){}, hidden = 0) {
    super (scene, x, y)
    this.scene = scene
    this.scene.add.existing(this)
    this._hidden = hidden
    this.cb = cb
    this.emptySprite = new CardEmptySprite(this.scene, 0, 0).setInteractive()
    this.hitZone = this.scene.add.zone(-35, -47.5, 70, 95).setOrigin(0).setInteractive()
    .on('pointerdown', this.startDrag.bind(this))
    .on('pointerup', () => this.cards.forEach(x=>x.clearTint()))
    this.g = this.scene.add.graphics()
    this.add([this.emptySprite, this.hitZone, this.g])
    this.cards = []
  }

  startDrag (pointer) {
    let index = null
    let faced = this.length - this._hidden 
    let y = pointer.downY - this.y - this.hitZone.y
    if (faced == 1) {
      index = this.length - 1
    } else {
      let m = (faced - 1) * 19
      if (y < m) {
        index = this._hidden + Math.floor(y / 19)
      } else {
        index = this.length - 1
      }
    }

    
    if (index == this.length-1) {
      let card = this.cards.splice(index, this.length)[0]
      let offset = {x: pointer.downX - this.x, y: pointer.downY - this.y - card.y}
      let response = {
        reset: {x: this.x + card.x, y: this.y + card.y},
        cards: [card.card],
        owner: this
      }
      card.destroy()
      this.update()
      this.cb(response, offset)
    } else {
      if (this.canDrag(index)) {
        let cards = this.cards.splice(index, this.length)
        let offset = {x: pointer.downX - this.x, y: pointer.downY - this.y - cards[0].y}
        let response = {
          reset: {x: this.x + cards[0].x, y: this.y + cards[0].y},
          cards: cards.map(card=>card.card),
          owner: this
        }
        cards.forEach(card=>card.destroy())
        this.update()
        this.cb(response, offset)
      }
    }
  }

  canDrag (index) {
    let suit = this.cards[index].suit
    for (let i = index; i < this.length-1; i++) {
      if ((this.cards[i].rank != this.cards[i+1].rank + 1) || (this.cards[i+1].suit != suit)) {
        return false
      }
    }
    return true
  }

  get hidden () {
    return this._hidden
  }

  openCard () {
    this._hidden--
    for (let i = 0; i<this.length; i++) {
      if (i < this._hidden) {
        this.cards[i].setHidden(true)
      } else {
        this.cards[i].setHidden(false)
      }
    }
  }

  get length () {
    return this.cards.length
  }

  push(rank) {
    let index = this.cards.length
    let card = new CardSprite(this.scene, 0, this.getOffset(index), rank, index < this._hidden)
    this.cards.push(card)
    this.add(card)
    this.update()
  }

  positive (x) {
    return x >= 0 ? x : 0
  }

  update () {
    this.hitZone.setPosition(-35, this.getOffset(this._hidden) - 47.5)
    this.hitZone.setSize(70, 95 + this.positive(this.length - this._hidden - 1) * 19)
    this.g.clear()
    this.g.lineStyle(2, 0xff0000)
    this.g.strokeRect(-35, this.getOffset(this._hidden) - 47.5, 70, 95 + this.positive(this.length - this._hidden - 1) * 19)
    this.bringToTop(this.g)
  }

  getOffset (index) {
    let high = 0
    if (index <= this._hidden) {
      high = index * 6
    } else {
      high = this._hidden * 6 + (index - this._hidden) * 19
    }
    return high
  }

  topPoint (index = 0) {
    return {x: this.x, y: this.y + this.getOffset(index)}
  }
}
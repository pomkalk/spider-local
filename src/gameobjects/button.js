import Phaser from 'phaser'

export default class Button extends Phaser.GameObjects.Container {
  constructor (scene, x, y, text, cb = function(){}) {
    super (scene, x, y)
    this.scene = scene
    this.g = this.scene.add.graphics()
    this.hitArea = this.scene.add.zone(0, 0, 180, 32).setInteractive()
    .on('pointerover', this.onOver.bind(this)).on('pointerout', this.onOut.bind(this)).on('pointerdown', this.onDown.bind(this)).on('pointerup', this.onUp.bind(this))
    this.label = this.scene.add.text(0, 0, text, {fontSize: 24, fixedWidth: 180, foxedHeight: 32, align: 'center'}).setOrigin(0.5)
    this.add([this.g, this.hitArea, this.label])
    this.cb = cb
    this.scene.add.existing(this)
    this.draw()
  }
  draw () {
    this.g.clear()
    this.g.lineStyle(2, 0xffffff)
    this.g.strokeRoundedRect(-90, -16, 180, 32, 5)
    this.label.setColor('#ffffff')
  }
  onOver () {
    this.g.clear()
    this.g.fillStyle(0xffffff)
    this.g.fillRoundedRect(-90, -16, 180, 32, 5)
    this.label.setColor('#000000')
  }
  onOut () {
    this.draw()
  }
  onDown () {
    this.g.clear()
    this.g.fillStyle(0xffffff)
    this.g.fillRoundedRect(-87, -13, 174, 26, 5)
    this.label.setColor('#000000')
  }
  onUp (pointer) {
    this.g.clear()
    this.g.fillStyle(0xffffff)
    this.g.fillRoundedRect(-90, -16, 180, 32, 5)
    this.label.setColor('#000000')
    this.cb()
  }
}
import Phaser from 'phaser'

export default class DebugGroup extends Phaser.GameObjects.Container {
  constructor (scene, x, y, cbs) {
    super(scene, x, y)
    this.scene = scene
    this.position = {x, y}
    this.scene.add.existing(this)
    this.cbs = Object.assign({red: function () {console.log('red clicked')}, green: function () {console.log('green clicked')}, blue: function () { console.log('blue clicked')}
    }, cbs)
    this.red = this.scene.add.circle(-30, 0, 15, 0xff0000, 0.5).setInteractive()
      .on('pointerover', () => this.red.setFillStyle(0xff0000, 1))
      .on('pointerout', () => this.red.setFillStyle(0xff0000, 0.5))
      .on('pointerup', this.cbs.red)
    this.green = this.scene.add.circle(0, 0, 15, 0x00ff00, 0.5).setInteractive()
      .on('pointerover', () => this.green.setFillStyle(0x00ff00, 1))
      .on('pointerout', () => this.green.setFillStyle(0x00ff00, 0.5))
      .on('pointerup', this.cbs.green)
    this.blue = this.scene.add.circle(30, 0, 15, 0x0000ff, 0.5).setInteractive()
      .on('pointerover', () => this.blue.setFillStyle(0x0000ff, 1))
      .on('pointerout', () => this.blue.setFillStyle(0x0000ff, 0.5))
      .on('pointerup', this.cbs.blue)
    this.add([this.red, this.green, this.blue])
  }
}
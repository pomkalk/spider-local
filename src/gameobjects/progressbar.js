import Phaser from 'phaser'

export default class ProgressBar extends Phaser.GameObjects.Container {
  constructor (scene, x, y, config) {
    super(scene, x, y)
    this.config = Object.assign({
      value: 0,
      max: 100,
      step: 1
    }, config)
    this.scene = scene
    this.scene.add.existing(this)
    this.border = this.scene.add.graphics()
    this.barShape = this.scene.make.graphics()
    this.bar = this.scene.add.graphics()
    this.border.lineStyle(2, 0xffffff)
    this.border.strokeRoundedRect(this.x - 120, this.y - 17, 240, 34, 5)
    this.barShape.fillStyle(0xffffff)
    this.barShape.fillRoundedRect(this.x - 117, this.y - 14, 234, 28, 5)
    this.barMask = this.barShape.createGeometryMask()
    this.bar.setMask(this.barMask)
    this.label = this.scene.add.text(-120, 28, this.getLabel(), { fixedWidth: 240, align: 'center', fontFamily: 'Arial' })
    this.add(this.label)
    this.draw()
  }
  getLabel () {
    return this.getPercent() + ' %'
  }
  getPercent () {
    return Math.ceil(this.config.value * 100 / this.config.max)
  }
  getBarSize () {
    let barSize = 234 * this.getPercent() / 100
    return barSize > 234 ? 234 : barSize
  }
  draw () {
    this.bar.clear()
    this.bar.fillStyle(0xffffff)
    this.bar.fillRect(this.x - 117, this.y - 14, this.getBarSize(), 50, 5)
    this.label.setText(this.getLabel())
  }
  setValue (value) {
    this.config.value = value <= this.config.max ? value : this.config.max
    this.draw()
  }
}

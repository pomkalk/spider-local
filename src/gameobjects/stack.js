import Phaser from 'phaser'
import CardBackSprite from '../sprites/CardBackSprite'

export default class Stack extends Phaser.GameObjects.Container {
  /**
   *Creates an instance of Stack.
   * @param {*} scene Parent scene
   * @param {*} x X position
   * @param {*} y Y position
   * @param {*} cardsCount Count of visible cards
   * @memberof Stack
   */
  constructor (scene, x, y, cardsCount, cb = function(){}) {
    super(scene, x, y)
    this.scene = scene
    this.scene.add.existing(this)
    this._space = 3
    this.cards = []
    for (let i = 0; i < cardsCount; i++) {
      this.cards.push(new CardBackSprite(this.scene, -this._space * i, 0))
    }
    this.add(this.cards)
    this.hitZone = this.scene.add.zone(0, 0, 70, 90).setOrigin(0).setInteractive().on('pointerup', cb)
    this.add(this.hitZone)
    this._updateZone()
  }

  pushCard () {
    this.pushCards(1)
  }

  pushCards (count = 1) {
    let l = this.cards.length
    for (let i = l; i < l + count; i++) {
      let card = new CardBackSprite(this.scene, -this._space * i, 0)
      this.cards.push(card)
      this.add(card)
    }
    this._updateZone()
  }

  get length () {
    return this.cards.length
  }

  popCard () {
    if (this.cards.length > 0) {
      let card = this.cards.pop()
      card.destroy()
    }
    this._updateZone()
  }

  _updateZone () {
    this.hitZone.setPosition(-30 - this._space * this.cards.length, -47.5)
    this.hitZone.setSize(65 + this._space * this.cards.length, 95)
  }

  get topPoint () {
    if (this.cards.length > 0) {
      let card = this.cards[this.cards.length - 1]
      return {x: this.x + card.x, y: this.y + card.y}
    } else {
      return {x: this.x, y: this.y}
    }
  }
}
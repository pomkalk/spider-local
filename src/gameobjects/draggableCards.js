import Phaser from 'phaser'
import CardSprite from '../sprites/Card'

export default class DraggableCards extends Phaser.GameObjects.Container {
  constructor (scene, config) {
    super(scene, config.reset.x, config.reset.y)
    this.scene = scene
    this.scene.add.existing(this)
    this.reset = config.reset
    this.owner = config.owner
    this.cards = []
    for (let i in config.cards) {
      this.cards.push(new CardSprite(this.scene, 0, i * 19, config.cards[i], false))
    }
    this.g = this.scene.add.graphics()
    this.add(this.cards)
    this.add(this.g)
    this.g.fillStyle(0x00ffff)
    this.g.fillCircle(-35,-47.5,3)
  }
  getCards () {
    return this.cards.map(card=>card.card)
  }
}
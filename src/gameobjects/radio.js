import Phaser from 'phaser'

export default class Radio extends Phaser.GameObjects.Container {
  constructor (scene, x, y, text, value, active, cb = function(){}) {
    super (scene, x, y)
    this.scene = scene
    this.value = value
    this.active = active
    this.cb = cb
    this.g = this.scene.add.graphics()
    this.g2 = this.scene.add.graphics()
    this.label = this.scene.add.text(-50,0,text).setOrigin(0, 0.5)
    this.hitArea = this.scene.add.zone(0, 0, 180, 32).setInteractive()
    .on('pointerover', () => {
      this.g2.fillStyle(0xffffff, 0.3)
      this.g2.fillCircle(-70, 0, 7)
    }).on('pointerout', () => this.g2.clear()).on('pointerup', this.click.bind(this))
    this.add([this.g, this.g2, this.label, this.hitArea])
    this.scene.add.existing(this)
    this.draw()
  }
  draw () {
    this.g.clear()
    this.g.lineStyle(2, 0xffffff)
    this.g.strokeCircle(-70, 0, 10)
    if (this.active) {
      this.g.fillStyle(0xffffff)
      this.g.fillCircle(-70, 0, 7)
    }
  }
  click () {
    this.cb(this)
  }
  setActive(value) {
    this.active = value
    this.draw()
  }
}
import Phaser from 'phaser'

export default class BootScene extends Phaser.Scene {
  constructor () {
    super ('Boot')
  }
  preload () {
    this.load.image('background', './src/assets/green_felt.jpg')
    this.load.bitmapFont('LogoFont', './src/assets/font.png', './src/assets/font.fnt')
  }
  create () {
    this.scene.start('Preloader')
  }
}

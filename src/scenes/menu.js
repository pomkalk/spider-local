import Phaser from 'phaser'
import Button from '../gameobjects/button'
import Radio from '../gameobjects/radio'
import BackgroundImage from '../sprites/BackgroundImage'

export default class MenuScene extends Phaser.Scene {
  constructor () {
    super ('Menu')
  }
  create () {
    this.bg = new BackgroundImage(this, 1024 / 2, 768 / 2)
    this.logo = this.add.bitmapText(1024 / 2, 768 / 3, 'LogoFont', 'Spider Game', 140, 1).setOrigin(0.5)

    this.playButton = new Button(this, 1024/2, 768/2, 'Play', () => {
      this.scene.start('Game', { gameType: localStorage.getItem('selectedSuit') || 'one' })
    })
    this.suits = {}
    this.suits['one'] = new Radio(this, 1024 / 2, 768 / 2 + 42, 'One suit', 'one', false, this.suitSelect.bind(this))
    this.suits['two'] = new Radio(this, 1024 / 2, 768 / 2 + 42 * 2, 'Two suits', 'two', false, this.suitSelect.bind(this))
    this.suits['four'] = new Radio(this, 1024 / 2, 768 / 2 + 42 * 3, 'Four suits', 'four', false, this.suitSelect.bind(this))
    this.suits[localStorage.getItem('selectedSuit')||'one'].setActive(true)
    this.scoreButton = new Button(this, 1024 / 2, 768 / 2 + 42*4, 'Scores', () => {
      this.scene.start('Scores')
    })
  }
  suitSelect (radio) {
    Object.values(this.suits).forEach(radio => radio.setActive(false))
    localStorage.setItem('selectedSuit', radio.value)
    radio.setActive(true)
  }
}

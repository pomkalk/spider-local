import Phaser from 'phaser'
import Button from '../gameobjects/button'
import BackgroundImage from '../sprites/BackgroundImage'

export default class ScoresScene extends Phaser.Scene {
  constructor () {
    super ('Scores')
  }
  create () {
    this.bg = new BackgroundImage(this, 1024 / 2, 768 / 2)
    this.logo = this.add.bitmapText(1024 / 2, 50, 'LogoFont', 'Scores', 72, 1).setOrigin(0.5, 0)
    this.back = new Button(this, 1024/2, 720, 'Go back', () => {
      this.scene.start('Menu')
    })
  }
}
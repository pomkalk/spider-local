import Phaser from 'phaser'
import ProgressBar from '../gameobjects/progressbar';
import BackgroundImage from '../sprites/BackgroundImage'

export default class PreloaderScene extends Phaser.Scene {
  constructor () {
    super ('Preloader')
  }
  preload () {
    this.load.image('cardBack', './src/assets/cards/cardBack_blue2.png');
    this.load.image('cardEmpty', './src/assets/cards/cardEmpty.png');
    this.load.image('ttt', './src/assets/cards/cardBack_green2.png');
    ['Clubs', 'Hearts', 'Diamonds', 'Spades'].forEach((suit) => {
      ['2','3','4','5','6','7','8','9','T','J','Q','K','A'].forEach((rank) => {
        this.load.image('card' + suit + rank, './src/assets/cards/card' + suit + rank + '.png')
      })
    })
    this.bg = new BackgroundImage(this, 1024/2, 768/2)
    this.logo = this.add.bitmapText(1024 / 2, 768 / 3, 'LogoFont', 'Spider Game', 140, 1).setOrigin(0.5)
    this.progress = new ProgressBar(this, 1024 / 2, 768 / 2)
    this.load.on('progress', (value) => {
      this.progress.setValue(value * 100)
    })
    this.load.on('complete', () => {
      this.scene.start('Menu')
    })
  }
}

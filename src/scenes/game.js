import Phaser from 'phaser'
import async from 'async'
import DebugGroup from '../gameobjects/debugGroup'
import Stack from '../gameobjects/stack'
import CardSprite from '../sprites/Card'
import BackgroundImage from '../sprites/BackgroundImage'
import CardEmptySprite from '../sprites/CardEmptySprite'
import Heap from '../gameobjects/heap'
import DraggableCards from '../gameobjects/draggableCards'


export default class GameScene extends Phaser.Scene {
  constructor () {
    super('Game')
    this.drag = null
    this.lock = true
  }

  create (data) {
    this.bg = new BackgroundImage(this, 1024 / 2, 768 / 2)
    this.debug = new DebugGroup(this, 50, 720, {
      red: () => this.stack.pushCard(),
      green: () => this.stack.popCard(),
      blue: () => {
        this.heaps[0]._hidden = 1
        this.heaps[0].cards[1].setHidden(false)
        let a = this.heaps[0].cards.splice(2, this.heaps[0].cards.length)
        a.forEach(x => this.heaps[1].push(x.card))
        a.forEach(x => x.destroy())
        this.heaps[0].update()
      }
    })
    this.stack = new Stack(this, 954, 683, 11, () => {
      if (!this.lock && this.pack.length > 0) {
        this.lock = true
        this.deal()
      }
    })
    this.heaps = []
    for (let i=0; i<10;i++){
      this.heaps.push(new Heap(this, 102 + i * 90, 80, (response, offset) =>{
        this.drag = new DraggableCards(this, response)
        this.offset = offset
      }, i<4?5:4))
    }
    this.pack = this.generatePack(data.gameType)

    async.eachOf([10, 10, 10, 10, 10, 4], (chunkCount, j, nextChunk) => {
      let chunk = this.pack.splice(0, chunkCount)
      let startPosition = this.stack.topPoint
      async.times(chunkCount, (i, nextI) => {
        let card = new CardSprite(this, -100, 0, chunk[i], true)
        let endPosition = this.heaps[i].topPoint(j)
        this.tweens.add({
          targets: card,
          duration: 200,
          delay: 50 * i + 250 * j,
          props: {
            x: { from: startPosition.x, to: endPosition.x },
            y: { from: startPosition.y, to: endPosition.y }
          },
          onStart: () => {
            if (i == chunkCount-1) {
              this.stack.popCard()
            }
          },
          onComplete: () => {
            card.destroy()
            this.heaps[i].push(chunk[i])
          }
        })
        nextI()
      }, () => {
        nextChunk()
      })
    }, () => {
      this.lock = false
    })
    this.input.on('pointerdown', (pointer) => {

    })
    this.input.on('pointermove', (pointer) => {
      if (this.drag) {
        this.drag.x = pointer.x - this.offset.x
        this.drag.y = pointer.y - this.offset.y
      }
    })
    this.input.on('pointerup', () => {
      if (this.drag) {
        if (this.dragCollide()) {

        } else {
          this.lock = true
          this.tweens.add({
            targets: this.drag,
            duration: 100,
            props: {
              x: {
                from: this.drag.x,
                to: this.drag.reset.x
              },
              y: {
                from: this.drag.y,
                to: this.drag.reset.y
              }
            },
            onComplete: () => {
              this.drag.getCards().forEach(card => this.drag.owner.push(card))
              this.drag.destroy()
              this.drag = null
              this.lock = false
            }
          })
        }
      }
    })
  }

  dragCollide () {
    let collusion = []
    this.heaps.forEach(heap => {
      if (heap != this.drag.owner) {
        console.log(this.drag.y - 47.5, heap.hitZone.y + heap.y, heap.hitZone.y + heap.hitZone.width + heap.y)
        if (((this.drag.y - 47.5 > heap.hitZone.y + heap.y) && (this.drag.y - 47.5 < heap.hitZone.y + heap.hitZone.width + heap.y)) ||
        ((this.drag.y + 47.5 + (this.drag.cards.length - 1) * 19 > heap.hitZone.y + heap.y) && (this.drag.y + 47.5 + (this.drag.cards.length - 1) * 19 < heap.hitZone.y + heap.hitZone.width + heap.y))) {
          console.log('asd')
        }
      }
    })
    return false
  }

  deal () {
    let chunk = this.pack.splice(0, 10)
    let startPosition = this.stack.topPoint
    async.times(10, (i, nextI) => {
      let card = new CardSprite(this, -100, 0, chunk[i], false)
      let endPosition = this.heaps[i].topPoint(this.heaps[i].length)
      this.tweens.add({
        targets: card,
        duration: 200,
        delay: 50 * i,
        props: {
          x: {
            from: startPosition.x,
            to: endPosition.x
          },
          y: {
            from: startPosition.y,
            to: endPosition.y
          }
        },
        onStart: () => {
          if (i == 9) {
            this.stack.popCard()
            this.lock = false
          }
        },
        onComplete: () => {
          card.destroy()
          this.heaps[i].push(chunk[i])
        }
      })
      nextI()
    })
  }

  generatePack (type) {
    let pack = []
    let suits = null
    switch (type) {
      case 'one': suits = 'ssssssss'; break;
      case 'two': suits = 'shshshsh'; break;
      case 'four': suits = 'shdcshdc'; break;
    }
    for (let suit of suits) {
      ['2', '3', '4', '5', '6', '7', '8', '9', 't', 'j', 'q', 'k', 'a'].forEach(rank => {
        pack.push(suit+rank)
      })
    }
    let tmp = null
    for (let i = pack.length - 1; i > 0; i--){
      let j = Math.floor(Math.random() * i)
      tmp = pack[i]
      pack[i] = pack[j]
      pack[j] = tmp
    }
    return pack
  }
}
import Phaser from 'phaser'

export default class CardSprite extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, card, hidden = false) {
    super(scene, x, y, hidden ? 'cardBack': CardSprite.getCardTexture(card))
    this.scene = scene
    this.card = card
    this._rank = this.getRank(card)
    this._suit = card[0]
    this._hidden = hidden
    this.scene.add.existing(this)
    this.setOrigin(0.5)
    this.setDisplaySize(70, 95)
  }

  get rank () {
    return this._rank
  }

  get suit () {
    return this._suit
  }

  getRank (rank) {
    return ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'].indexOf(rank[1].toUpperCase())
  }

  get hidden () {
    return this._hidden
  }

  setHidden(hidden) {
    if (hidden) {
      this.setTexture('cardBack')
    } else {
      this.setTexture(CardSprite.getCardTexture(this.card))
    }
    this._hidden = hidden
  }

  static getCardTexture (card) {
    let c = card[0]
    let suit = ''
    switch (c.toLowerCase()) {
      case 'c': suit = 'Clubs'; break;
      case 'd': suit = 'Diamonds'; break;
      case 'h': suit = 'Hearts'; break;
      case 's': suit = 'Spades'; break;
    }
    return 'card' + suit + card[1].toUpperCase()
  }
}
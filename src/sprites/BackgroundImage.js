import Phaser from 'phaser'

export default class BackgroundImage extends Phaser.GameObjects.Image {
  constructor(scene, x, y) {
    super(scene, x, y, 'background')
    this.scene = scene
    this.scene.add.existing(this)
    this.setOrigin(0.5)
    this.setDisplaySize(1024, 768)
  }
}
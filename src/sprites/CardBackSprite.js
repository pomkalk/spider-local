import Phaser from 'phaser'

export default class CardBackSprite extends Phaser.GameObjects.Sprite {
  constructor (scene, x, y) {
    super (scene, x, y, 'cardBack')
    this.scene = scene
    this.scene.add.existing(this)
    this.setOrigin(0.5)
    this.setDisplaySize(70, 95)
  }
}
import Phaser from 'phaser'

export default class CardEmptySprite extends Phaser.GameObjects.Sprite {
  constructor (scene, x, y) {
    super (scene, x, y, 'cardEmpty')
    this.scene = scene
    this.scene.add.existing(this)
    this.setOrigin(0.5)
    this.setDisplaySize(70, 95)
    this.setAlpha(0.7)
  }
}